import re

from quest_script.classes import Utils


def street_init(context: dict, utils: Utils):
    return {"answer_text": "Вы на улице"}


def street_input_handler(context: dict, utils: Utils, answer: str):
    if "freezing" not in context:
        context['freezing'] = 0
    answer = answer.lower().strip()
    if re.fullmatch(".*осм[оа]треть(ся)?.*", answer):
        utils.print("Вы видите центр небольшого городка, пару зданий, магазин и отель.\n"
                    "На улице ещё довольно светло. ")
        if not context.get("wearing_warm_clothes", False):
            utils.print("Одеты вы явно не по погоде, слегка замерзаете.\n"
                        "В кармане чувствуется пропуск в отель, возможно стоит вернуться и переодеться")
    elif re.fullmatch(".*((пой|вый|прой)(ти|ду)(сь)?)\s+(на|в|по|про)\s*(отель|номер|гостин).*", answer):
        context['freezing'] = 0
        utils.print("Вы дошли до отеля, улыбнулись роботу на рецепшене и проскочили в свой номер.\n"
                    "Снимая рюкзак, заметили ноутбук создателя с зашифрованным роботом. Ужасно захотелось покопаться")
        return {"change_state": "hotel"}
    elif re.fullmatch(".*(вызвать|поймать|ловить)\s+(такси|машину|тачку).*", answer):
        context['freezing'] = 0
        utils.print("К вам подъехало беспилотное такси и просит ввести точку назначения.")
        return {"change_state": "taxi"}
    elif re.fullmatch(".*((пой|вый|прой)(ти|ду)(сь)?)\s+(на|в|по|про)\s*(магазин|продукт).*", answer):
        context['freezing'] = 0
        utils.print("Вы зашли в магазин. Чувствуете небольшой голод.")
        return {"change_state": "shop"}
    elif re.fullmatch(".*((пой|вый|прой|прогул)(ти|ду|яю|лять)(сь|ся)?)\s+(на|в|по|про)\s*(улиц|руж|возд|гул).*",
                      answer):
        if not context.get("wearing_warm_clothes", False):
            if context['freezing'] < 3:
                utils.print("Вам холодно но весело. Замерзнете вы не скоро, но переодеться стоит.")
            elif context['freezing'] < 6:
                utils.print("Вы довольно замерзли. Стоит переодеться. \n"
                            "Но вам кажется что скоро вы найдете что-то интересное.")
            elif context['freezing'] >= 6:
                utils.print("Вы нашли интересную особенность, от холода тело погибает. Игра окончена.")
                return
            context['freezing'] += 1
        else:
            utils.print("Вам очень нравится свежий воздух")
    else:
        utils.print("Нераспознанное действие, повторите, пожалуйста, ввод")


def street_deinit(context: dict, utils: Utils):
    return {"answer_text": "Вы покинули улицу"}
