import re
from dataclasses import dataclass
from random import randint
from typing import Callable


@dataclass()
class Funs:
    print: Callable[[str], None]
    input: Callable[[], str]


def taxi(context: dict, funs: Funs):
    pass


def encrypt(funs):
    funs.print(
        "Для расшифровки нужно подобрать пароль. Однако, после долгого анализа вы заметили несколько особенностей:"
        "Код состоит из 6-ти цифр, и каждый раз после новой попытки вы можете узнать,\n"
        "сколько цифр из написанных Вами есть в коде и сколько цифр стоит на своём месте. Очень удобная информация! "
        "Попробуйте угадать код, у Вас есть 5 попыток")

    code = tuple((randint(0, 9) for i in range(6)))

    funs.print(code)

    answer = funs.input()
    if answer == "отменить":
        return "canceled"

    input_code = tuple((int(i) for i in answer))

    if code == input_code:
        return "encrypted"
    else:
        return "failed"


def work_with_laptop(context: dict, funs: Funs):
    answer = funs.input()
    if answer == "попытаться расшифровать":
        encrypt_result = encrypt(funs)
        if encrypt_result == "encrypted":
            funs.print("Успешно расшифровали")
        elif encrypt_result == "canceled":
            funs.print("Вы отменили расшифровку")
        elif encrypt_result == "failed":
            funs.print("Не вышло, данные удалены. Игра окончена")
            return {"action": "exit"}
    elif answer == "закрыть ноутбук":
        funs.print('вы закрыли и убрали ноутбук')
        return {"change_state": context.get("laptop_close_state")}
    else:
        funs.print("Нераспознанное действие, повторите, пожалуйста, ввод")


def hotel(context: dict, funs: Funs):
    answer = funs.input().lower().strip()
    if re.fullmatch(".*осм[оа]треть(ся)?.*", answer):
        funs.print("Вы в номере отеля. Душ, туалет, кровать, стол. Что ещё надо для хорошей жизни?")
    elif re.fullmatch(".*(открыть|включить|достать)\s+ноутбук.*", answer):
        funs.print("Вы достали, открыли и включили ноутбук.\n"
                   "Вас приветствует окно, в котором написано что данные зашифрованы и для разблокировки надо отправить 3 биткойна на некий кошелек.\n"
                   "На этом окне есть странные логотипы, которые кажутся смутно знакомыми.")
        context["laptop_close_state"] = "hotel"
        return {"change_state": "work_with_laptop"}
    elif re.fullmatch(".*((пой|вый|прой)(ти|ду)(сь)?)\s+(на|в|по|про|из)\s*(улиц|руж|возд|гул|отел|гост|номер).*",
                      answer):
        return {"change_state": "street"}
    elif re.fullmatch(".*переодеться.*", answer):
        if context.get("wearing_warm_clothes", False):
            funs.print("Вы одели одежду потеплее")
            context["wearing_warm_clothes"] = True
        else:
            funs.print("Вы сняли одежду потеплее")
            context["wearing_warm_clothes"] = False
    else:
        funs.print("Нераспознанное действие, повторите, пожалуйста, ввод")


def shop(context: dict, funs: Funs):
    if context.get("shop_state", "main") == "main":
        funs.print("В магазине вы видите 3 отдела. Молоко, мясо и хлеб.")
        answer = funs.input().lower().strip()
        if re.fullmatch(".*((пой|прой|подой)(ти|ду)(сь)?)\s+(на|в|по|про|к)\s*(отдел\S*)?\s+молок.*", answer):
            funs.print("вы прошли в молочный отдел")
            context["shop_state"] = "milk_department"
        elif re.fullmatch(".*((пой|прой|подой)(ти|ду)(сь)?)\s+(на|в|по|про|к)\s*(отдел\S*)?\s+хлеб.*", answer):
            funs.print("вы прошли в хлебобулочный отдел")
            context["shop_state"] = "bread_department"
        elif re.fullmatch(".*((пой|прой|подой)(ти|ду)(сь)?)\s+(на|в|по|про|к)\s*(отдел\S*)?\s+мяс.*", answer):
            funs.print("вы прошли в мясной отдел")
            context["shop_state"] = "meat_department"
    elif context.get("shop_state") in ("milk_department", "bread_department", "meat_department"):
        department_info = {
            "milk_department": {
                "name": "молочный отдел",
                "goods": (
                    {"name": "молоко", "price": 50, "desc": "Обычное молоко"},
                    {"name": "сыр", "price": 300, "desc": "Кусок сыра"},
                )
            },
            "bread_department": {
                "name": "хлебобулочный отдел",
                "goods": (
                    {"name": "хлеб черный", "price": 30, "desc": "Чернее ночи"},
                    {"name": "хлеб белый", "price": 25, "desc": "Белее дня"},
                    {"name": "булочка сдобная", "price": 75, "desc": "Просто булочка"},
                )
            },
            "meat_department": {
                "name": "мясной отдел",
                "goods": (
                    {"name": "колбаса варенная", "price": 300, "desc": "Ммммм, докторская"},
                    {"name": "колбаса копченная", "price": 500, "desc": "Пахнет неплохо"},
                )
            }
        }
        cur_department_info = department_info[context['shop_state']]
        cur_department_name = cur_department_info["name"]
        funs.print(f"Это {cur_department_name}. Ваши действия?")
        answer = funs.input().lower().strip()
        if re.fullmatch(".*купить\s\w+.*", answer):
            good_name = re.search("купить\s\w+", answer).group()
            good_name = re.sub("купить\s", '', good_name)

            funs.print(f"Вы хотите купить {good_name}")


def street(context: dict, funs: Funs):
    if "freezing" not in context:
        context['freezing'] = 0
    answer = funs.input().lower().strip()
    if re.fullmatch(".*осм[оа]треть(ся)?.*", answer):
        funs.print("Вы видите центр небольшого городка, пару зданий, магазин и отель.\n"
                   "На улице ещё довольно светло. ")
        if not context.get("wearing_warm_clothes", False):
            funs.print("Одеты вы явно не по погоде, слегка замерзаете.\n"
                       "В кармане чувствуется пропуск в отель, возможно стоит вернуться и переодеться")
    elif re.fullmatch(".*((пой|вый|прой)(ти|ду)(сь)?)\s+(на|в|по|про)\s*(отель|номер|гостин).*", answer):
        context['freezing'] = 0
        funs.print("Вы дошли до отеля, улыбнулись роботу на рецепшене и проскочили в свой номер.\n"
                   "Снимая рюкзак, заметили ноутбук создателя с зашифрованным роботом. Ужасно захотелось покопаться")
        return {"change_state": "hotel"}
    elif re.fullmatch(".*(вызвать|поймать|ловить)\s+(такси|машину|тачку).*", answer):
        context['freezing'] = 0
        funs.print("К вам подъехало беспилотное такси и просит ввести точку назначения.")
        return {"change_state": "taxi"}
    elif re.fullmatch(".*((пой|вый|прой)(ти|ду)(сь)?)\s+(на|в|по|про)\s*(магазин|продукт).*", answer):
        context['freezing'] = 0
        funs.print("Вы зашли в магазин. Чувствуете небольшой голод.")
        return {"change_state": "shop"}
    elif re.fullmatch(".*((пой|вый|прой|прогул)(ти|ду|яю|лять)(сь|ся)?)\s+(на|в|по|про)\s*(улиц|руж|возд|гул).*",
                      answer):
        if not context.get("wearing_warm_clothes", False):
            if context['freezing'] < 3:
                funs.print("Вам холодно но весело. Замерзнете вы не скоро, но переодеться стоит.")
            elif context['freezing'] < 6:
                funs.print("Вы довольно замерзли. Стоит переодеться. \n"
                           "Но вам кажется что скоро вы найдете что-то интересное.")
            elif context['freezing'] >= 6:
                funs.print("Вы нашли интересную особенность, от холода тело погибает. Игра окончена.")
                return
            context['freezing'] += 1
        else:
            funs.print("Вам очень нравится свежий воздух")
    else:
        funs.print("Нераспознанное действие, повторите, пожалуйста, ввод")


def coffee_house(context: dict, funs: Funs):
    answer = funs.input().lower().strip()  # методы str.upper() и str.lower() заменяют маленькие буквы на большие или большие на маленькие соответственно

    if re.fullmatch(".*осм[оа]треть(ся)?.*", answer):
        return {
            "answer_text": "Вы сидите и попиваете смузи около севера далекой планеты, находясь в местном аналоге кофейни.\n"
                           "Напротив сидит создатель, недалеко сидят пара местных гуманоидов, есть дверь на улицу"}
    elif re.fullmatch(".*((пой|вый|прой)(ти|ду)(сь)?)\s+(на|в|по|про)\s*(улиц|руж|возд|гул).*", answer):
        return {"answer_text": "Вы вышли на улицу. Довольно холодно. Что дальше?", "change_state": "street"}
    elif re.fullmatch(".*([оО]платить\s+(смузи|заказ))|([пП]опросить\s+сч[её]т)|([рР]асс?читать).*", answer):
        return {
            "answer_text": "Это модная кофейня с распознаванием лиц и без персонала. Оплата происходит автоматически"}
    elif re.fullmatch(".*говорить.*", answer):
        return {"answer_text": "Создатель очень хочет вернуть робота но не очень может помочь. Помогите ему!"}
    elif re.fullmatch(".*отказать.*", answer):
        return {"action": "exit", "answer_text": "Вас назвали бесчуственным и ушли. Игра окончена"}
    elif re.fullmatch(".*(открыть|включить|достать)\s+ноутбук.*", answer):
        text = "Вы достали, открыли и включили ноутбук.\n" \
               "Вас приветствует окно, в котором написано что данные зашифрованы и для разблокировки надо отправить 3 биткойна на некий кошелек.\n" \
               "На этом окне есть странные логотипы, которые кажутся смутно знакомыми."
        context["laptop_close_state"] = "coffee_house"
        return {"change_state": "work_with_laptop", "answer_text": text}
    else:
        return {"answer_text": "Нераспознанное действие, повторите, пожалуйста, ввод"}


state_machine = {
    "coffee_house": coffee_house,
    "street": street,
    "shop": shop,
    "hotel": hotel,
    "work_with_laptop": work_with_laptop,
}


def quest_init(context: dict, funs: Funs):
    text = "В нектором царстве жили были ботик и его создатель.\n" \
           "Однако однажды злой хакер зашифровал бота и создатель очень хочет его вернуть.\n" \
           "В поисках он находит Вас и просит помочь. Ваши действия?"

    return {"answer_text": text, "change_state": "coffee_house"}


def main(_input=input, _print=print):
    current_state = None
    context = {}
    funs = Funs(print=_print, input=_input)

    result = quest_init(context, funs)

    if result is not None:
        if "answer_text" in result:
            funs.print(result["answer_text"])
        if "change_state" in result:
            current_state = result["change_state"]

    while True:
        result = state_machine[current_state](context, funs)

        if result is not None:
            if "answer_text" in result:
                funs.print(result["answer_text"])
            if "change_state" in result:
                current_state = result["change_state"]
            if "action" in result:
                if result["action"] == "exit":
                    return


if __name__ == '__main__':  # если запустили именно этот файл
    main()
