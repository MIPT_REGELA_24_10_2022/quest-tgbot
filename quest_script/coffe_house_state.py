import re

from quest_script.classes import Utils


def coffee_house_init(context: dict, utils: Utils):
    return {"answer_text": "Вы в кофейне"}


def coffee_house_input_handler(context: dict, utils: Utils, answer: str):
    answer = answer.lower().strip()  # методы str.upper() и str.lower() заменяют маленькие буквы на большие или большие на маленькие соответственно

    if re.fullmatch(".*осм[оа]треть(ся)?.*", answer):
        return {
            "answer_text": "Вы сидите и попиваете смузи около севера далекой планеты, находясь в местном аналоге кофейни.\n"
                           "Напротив сидит создатель, недалеко сидят пара местных гуманоидов, есть дверь на улицу"}
    elif re.fullmatch(".*((пой|вый|прой)(ти|ду)(сь)?)\s+(на|в|по|про)\s*(улиц|руж|возд|гул).*", answer):
        return {"answer_text": "Вы вышли на улицу. Довольно холодно. Что дальше?", "change_state": "street"}
    elif re.fullmatch(".*([оО]платить\s+(смузи|заказ))|([пП]опросить\s+сч[её]т)|([рР]асс?читать).*", answer):
        return {
            "answer_text": "Это модная кофейня с распознаванием лиц и без персонала. Оплата происходит автоматически"}
    elif re.fullmatch(".*говорить.*", answer):
        return {"answer_text": "Создатель очень хочет вернуть робота но не очень может помочь. Помогите ему!"}
    elif re.fullmatch(".*отказать.*", answer):
        return {"action": "exit", "answer_text": "Вас назвали бесчуственным и ушли. Игра окончена"}
    elif re.fullmatch(".*(открыть|включить|достать)\s+ноутбук.*", answer):
        text = "Вы достали, открыли и включили ноутбук.\n" \
               "Вас приветствует окно, в котором написано что данные зашифрованы и для разблокировки надо отправить 3 биткойна на некий кошелек.\n" \
               "На этом окне есть странные логотипы, которые кажутся смутно знакомыми."
        context["laptop_close_state"] = "coffee_house"
        return {"change_state": "work_with_laptop", "answer_text": text}
    else:
        return {"answer_text": "Нераспознанное действие, повторите, пожалуйста, ввод"}


def coffee_house_deinit(context: dict, utils: Utils):
    return {"answer_text": "Вы вышли из кофейни"}
