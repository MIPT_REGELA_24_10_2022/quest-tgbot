from dataclasses import dataclass
from typing import Callable


@dataclass
class Utils:
    print: Callable[[str], None] = print


@dataclass
class QuestState:
    state_init: Callable[[dict, Utils], dict]
    state_input_handle: Callable[[dict, Utils, str], dict]
    state_deinit: Callable[[dict, Utils], dict]


@dataclass
class QuestStates:
    states: dict[str, QuestState]

    def __getattr__(self, item):
        return self.states[item]

    def get_state(self, item):
        return self.states[item]


class QuestScript:
    def __init__(self, states: QuestStates, start_state_name):
        self.states: QuestStates = states
        self.current_state = start_state_name
        self.context = {}
        self.utils = Utils()

    def handle_result(self, result, allow_change_state=True):
        next_state = self.handle_result_static(result, self.utils, self.states, self.current_state,
                                               self.context, allow_change_state)
        if next_state is None:
            return "EXIT"
        self.current_state = next_state
        return "OK"

    @staticmethod
    def handle_result_static(result, utils: Utils, states, current_state, context, allow_change_state=True):
        if result is not None:
            if "answer_text" in result:
                utils.print(result["answer_text"])
            if "change_state" in result and allow_change_state:
                deinit_result = states.get_state(current_state).state_deinit(context, utils)
                QuestScript.handle_result_static(deinit_result, utils, states, current_state, context,
                                                 allow_change_state=False)
                current_state = result["change_state"]
                init_result = states.get_state(current_state).state_init(context, utils)
                QuestScript.handle_result_static(init_result, utils, states, current_state, context,
                                                 allow_change_state=False)
            if "action" in result:
                if result["action"] == "exit":
                    return None
        return current_state

    def handle_input(self, text):
        return self.handle_input_static(text, self.utils, self.states, self.current_state, self.context)

    @staticmethod
    def handle_input_static(text, utils, states, current_state, context):
        return states.get_state(current_state).state_input_handle(context, utils, text)

    def start(self, utils: Utils, _input=input):
        self.utils = utils
        while True:
            result = self.handle_input(_input())
            if self.handle_result(result) == "EXIT":
                return
