from quest_script import QuestStates, QuestScript
from quest_script.classes import Utils, QuestState


def test_utils():
    assert isinstance(Utils(), Utils)
    messages = []
    _print = lambda str: messages.append(str)
    utils = Utils(print=_print)
    assert isinstance(utils, Utils)
    utils.print("PRINT_MESSAGE")
    assert len(messages) == 1
    assert messages[0] == "PRINT_MESSAGE"
    utils.print("PRINT_MESSAGE_2")
    assert len(messages) == 2
    assert messages[0] == "PRINT_MESSAGE"
    assert messages[1] == "PRINT_MESSAGE_2"


def test_question_state():
    messages = []

    def init(d, utils):
        messages.append("init")
        return {}

    def input_handle(d, utils, str):
        messages.append("input_handle: " + str)
        return {}

    def deinit(d, utils):
        messages.append("deinit")
        return {}

    qs = QuestState(init, input_handle, deinit)
    assert isinstance(qs, QuestState)
    assert qs.state_init is init
    assert qs.state_input_handle is input_handle
    assert qs.state_deinit is deinit
    qs.state_init({}, Utils())
    qs.state_input_handle({}, Utils(), "test")
    qs.state_deinit({}, Utils())
    assert len(messages) == 3
    assert messages[0] == "init"
    assert messages[1] == "input_handle: test"
    assert messages[2] == "deinit"


def test_quest_states():
    messages = []

    def init(d, utils):
        messages.append("init")
        return {}

    def input_handle(d, utils, str):
        messages.append("input_handle: " + str)
        return {}

    def deinit(d, utils):
        messages.append("deinit")
        return {}

    def init_2(d, utils):
        messages.append("init_2")
        return {}

    def input_handle_2(d, utils, str):
        messages.append("input_handle_2: " + str)
        return {}

    def deinit_2(d, utils):
        messages.append("deinit_2")
        return {}

    qs1 = QuestState(init, input_handle, deinit)
    qs2 = QuestState(init_2, input_handle_2, deinit_2)

    assert qs1 is not qs2

    qss = QuestStates({"state1": qs1, "state2": qs2})

    assert isinstance(qss, QuestStates)

    assert qss.state1 is qs1
    assert qss.get_state("state1") is qs1

    assert qss.state2 is qs2
    assert qss.get_state("state2") is qs2

    qss.state1.state_init({}, Utils())
    qss.state1.state_input_handle({}, Utils(), "test")
    qss.state1.state_deinit({}, Utils())

    qss.get_state("state2").state_init({}, Utils())
    qss.get_state("state2").state_input_handle({}, Utils(), "test_2")
    qss.get_state("state2").state_deinit({}, Utils())

    assert len(messages) == 6
    assert messages == ["init", "input_handle: test", "deinit", "init_2", "input_handle_2: test_2", "deinit_2"]


def test_quest_script():
    messages = []

    def init_1(d, utils):
        messages.append("init_1")
        return {}

    def input_handle_1(d, utils, str):
        if "history" not in d:
            d["history"] = []
        d["history"].append(str)
        messages.append("input_handle_1: " + str)
        if str == "go_to_2":
            return {"change_state": "state2"}
        if str == "additional_text":
            return {"answer_text": "state_1_additional_text"}
        if str == "exit":
            return {"action": "exit"}
        if str == "all":
            return {"change_state": "state2", "answer_text": "state_1_additional_text", "action": "exit"}
        return {}

    def deinit_1(d, utils):
        messages.append("deinit_1")
        return {}

    def init_2(d, utils):
        messages.append("init_2")
        return {"change_state": "state1"}

    def input_handle_2(d, utils, str):
        if "history" not in d:
            d["history"] = []
        d["history"].append(str)
        messages.append("input_handle_2: " + str)
        if str == "go_to_1":
            return {"change_state": "state1"}
        if str == "additional_text":
            return {"answer_text": "state_2_additional_text"}
        if str == "exit":
            return {"action": "exit"}
        if str == "all":
            return {"change_state": "state1", "answer_text": "state_2_additional_text", "action": "exit"}

    def deinit_2(d, utils):
        messages.append("deinit_2")

    qs1 = QuestState(init_1, input_handle_1, deinit_1)
    qs2 = QuestState(init_2, input_handle_2, deinit_2)

    qss = QuestStates({"state1": qs1, "state2": qs2})

    quest_script = QuestScript(qss, "state1")

    assert isinstance(quest_script, QuestScript)
    inputs = ["all", "go_to_1", "additional_text", "go_to_2", "additional_text"]
    quest_script.start(Utils(print=lambda str: messages.append(str)), lambda: inputs.pop())
    expectes_messages = [
        "input_handle_1: additional_text", "state_1_additional_text",
        "input_handle_1: go_to_2", "deinit_1", "init_2", "input_handle_2: additional_text",
        "state_2_additional_text", "input_handle_2: go_to_1", "deinit_2", "init_1", "input_handle_1: all",
        "state_1_additional_text", "deinit_1", "init_2",
    ]
    assert messages == expectes_messages
    assert "history" in quest_script.context
    assert quest_script.context["history"] == ['additional_text', 'go_to_2', 'additional_text', 'go_to_1', 'all']
