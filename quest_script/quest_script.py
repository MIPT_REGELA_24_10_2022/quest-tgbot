from . import street_state
from .classes import QuestStates, QuestState, QuestScript
from .coffe_house_state import coffee_house_init, coffee_house_input_handler, coffee_house_deinit
from .street_state import street_init, street_input_handler, street_deinit

states = QuestStates({
    "coffee_house": QuestState(coffee_house_init, coffee_house_input_handler, coffee_house_deinit),
    "street": QuestState(street_init, street_input_handler, street_deinit)
})

quest_script = QuestScript(states, "coffee_house")

