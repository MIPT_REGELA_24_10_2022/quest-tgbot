class Messages:
    def __init__(self, messages: dict[str, str]):
        self.messages: dict[str, str] = messages

    def get_message(self, message_name, *args, **kwargs):
        if message_name in self.messages:
            return self.messages[message_name].format(*args, **kwargs)


messages_dict = {
    "hello": "Здравствуйте! Предлагаю Вам пройти опрос!",
    "hello_2": "{first_name} {last_name}, Ваш id: {user_id}, Ваш логин: {user_name}",
    "not_understand": "Я вас не понимаю",
    "quest_started": "Квест начался",
    "quest_ended": "Квест окончен",
    "start_later": "Начнете когда сможете"
}

messages = Messages(messages_dict
                    )
