from quest_script import Utils
from quest_script import quest_script


def main():
    quest_script.start(Utils())


if __name__ == '__main__':
    main()
