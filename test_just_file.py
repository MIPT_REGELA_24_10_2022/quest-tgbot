import pytest

from just_file import func, func_2


def test_func():
    assert func(5) == 15
    assert func(-10) == 0
    assert func(10000) == 10010
    with pytest.raises(TypeError) as excinfo:
        func("str")

    assert str(excinfo.value) == 'can only concatenate str (not "int") to str'


def test_func_2():
    assert func_2("a") == "HELLO a"
    assert func_2("SomeValue") == "HELLO SomeValue"
    assert func_2("") == "HELLO "

    with pytest.raises(TypeError) as excinfo:
        func_2(10)
    assert str(excinfo.value) == 'can only concatenate str (not "int") to str'
