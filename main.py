import logging

from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.files import JSONStorage
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import ReplyKeyboardMarkup

from messages import messages as msgs
from quest_script import QuestScript, Utils, states
from settings import API_TOKEN

# Configure logging

logging.basicConfig(level=logging.DEBUG)

# Initialize bot and dispatcher

storage = JSONStorage("storage.json")

bot = Bot(token=API_TOKEN)

dp = Dispatcher(bot, storage=storage)


class States(StatesGroup):
    main_state = State()
    quest_is_on = State()


@dp.message_handler(commands=['start', 'help'], state="*")
async def send_welcome(message: types.Message, state: FSMContext):
    await States.main_state.set()
    markup = ReplyKeyboardMarkup(one_time_keyboard=True).add("Начать", "Позже")
    await message.reply(msgs.get_message("hello"), reply_markup=markup)
    await message.answer(msgs.get_message("hello_2", first_name=message.from_user.first_name,
                                          last_name=message.from_user.last_name,
                                          user_id=message.from_user.id,
                                          user_name=message.from_user.username))
    logging.info(f"Message from {message.from_user.username}: {message.text}")


@dp.message_handler(state=States.main_state)
async def main_state_handler(message: types.Message, state: FSMContext):
    if message.text == "Начать":
        await States.quest_is_on.set()
        await message.answer(msgs.get_message("quest_started"))
        async with state.proxy() as data:
            data['current_state'] = 'coffee_house'
    elif message.text == "Позже":
        markup = ReplyKeyboardMarkup(one_time_keyboard=True).add("Начать")
        await message.answer(msgs.get_message("start_later"), reply_markup=markup)
    else:
        await message.answer(msgs.get_message("not_understand"))

    logging.info(f"Message from {message.from_user.username}: {message.text}")


@dp.message_handler(state=States.quest_is_on)
async def quest_handler(message: types.Message, state: FSMContext):
    messages = []
    async with state.proxy() as data:
        utils = Utils(lambda text, m=messages: m.append(text))
        result = QuestScript.handle_input_static(message.text, utils, states, data['current_state'], data)
        QuestScript.handle_result_static(result, utils, states, data['current_state'], data)
    for msg in messages:
        await message.answer(msg)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
